
Epica Pixel Helper Extension for Chrome
=======

Displays the events captured in a website

Author: Miguel Arbeláez Torres
Content is licensed under the [Google BSD License](https://developers.google.com/open-source/licenses/bsd).
Version: 1.0.7

Calls
-----

* [browserAction.onClicked](https://developer.chrome.com/extensions/browserAction#event-onClicked)
* [debugger.attach](https://developer.chrome.com/extensions/debugger#method-attach)
* [debugger.detach](https://developer.chrome.com/extensions/debugger#method-detach)
* [debugger.onEvent](https://developer.chrome.com/extensions/debugger#event-onEvent)
* [debugger.sendCommand](https://developer.chrome.com/extensions/debugger#method-sendCommand)
* [runtime.lastError](https://developer.chrome.com/extensions/runtime#property-lastError)
* [windows.create](https://developer.chrome.com/extensions/windows#method-create)