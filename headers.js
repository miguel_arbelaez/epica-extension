// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var eventlist = ["Recommendations on Page","Recommender Displayed","Platform Opened", "Article Viewed", "Service Activated", "Service Updated", "Service Cancelled", "Balance Checked", "Balance Updated", "Chat Opened", "Button Clicked", "Page Viewed", "Home Viewed", "Booking", "Conversion", "Flight List Viewed", "Flight List Filtered", "Promotion Viewed", "Promotion Clicked", "Flight Viewed", "Flight Selected", "Seat Selected", "Checkout Started", "Checkout Step Viewed", "Checkout Step Completed", "Payment Info Entered", "Order Completed", "Order Updated", "Order Refunded", "Order Cancelled", "Coupon Entered", "Coupon Applied", "Coupon Denied", "Coupon Removed", "Flight Shared", "Hotel List Viewed", "Hotel List Filtered", "Hotel Clicked", "Hotel Viewed", "Hotel Selected", "Room Selected", "Hotel Shared", "Products Searched", "Product List Viewed", "Product List Filtered", "Product Clicked", "Product Viewed", "Product Added", "Product Removed", "Cart Viewed", "Coupon Entered", "Coupon Applied", "Coupon Denied", "Coupon Removed", "Product Added to Wishlist", "Product Removed from Wishlist", "Wishlist Product Added to Cart", "Product Shared", "Cart Shared", "Product Reviewed", "Video Playback Started", "Video Playback Paused", "Video Playback Interrupted", "Video Playback Buffer Started", "Video Playback Buffer Completed", "Video Playback Seek Started", "Video Playback Seek Completed", "Video Playback Resumed", "Video Playback Completed", "Video Content Started", "Video Content Playing", "Video Content Completed", "Program Selected", "Category Viewed", "Article Readed", "Product Detail Viewed", "Poll Completed", "Form Submitted", "AudienceMatching", "Datonics Categories", "Section Viewed", "Conversion", "Page Scrolled", "Page Shared", "Article Shared", "Social Link Clicked", "Linck Clicked", "Social Link Clicked", "Signed Up", "Logged In", "Logged Of", "Page Reviewed", "Article Reviewed", "Comment Sent", "Content Searched"];

var tabId = parseInt(window.location.search.substring(1));
var reloaded = false;
var hasevents = false;
var countpage = 0;
var totalcount = 0;
var createdelement = "";

var clusterlink = "https://platform.epica.ai/cluster/";

var reloadcount = 0;
var historiccount = 0;

var readytoberestored = true;

var write_key = "";
var epica_session = "";

var id="";
var key="";

var initial_reload = false;

var platformurl = "https://platform.epica.ai/";

chrome.tabs.onUpdated.addListener(function (tabId) {
    reloaded = true;
    readytoberestored = true;
});

function findingEvents() {
    document.getElementById('text-state').innerHTML = '<p class="searching">Searching Events<span class="ellipsis-anim"><span>.</span><span>.</span><span>.</span></span></p>';
}

function eventsFound() {
    document.getElementById('text-state').innerHTML = '<span>No events found.</span><br><button id="reloadBtnAgain">Try again</button>';
    document.getElementById('reloadBtnAgain').addEventListener('click', reloadMainTab);
}

window.addEventListener("load", function () {
    chrome.debugger.sendCommand({ tabId: tabId }, "Network.enable");
    chrome.debugger.onEvent.addListener(onEvent);
});

window.addEventListener("unload", function () {
    chrome.debugger.detach({ tabId: tabId });
});

var requests = {};
var staticurl = "";

chrome.tabs.onRemoved.addListener(function (tabClosed) {
    if (tabClosed == tabId) {
        chrome.tabs.getCurrent(function (tab) {
            chrome.tabs.remove(tab.id, function () { });
        });
    }
});

function onEvent(debuggeeId, message, params) {
    if (initial_reload) {
        totalcount++;

        if (tabId != debuggeeId.tabId) {
            return;
        }

        if (message == "Network.requestWillBeSent") {
            var requestDiv = requests[params.requestId];
            if (typeof params.request !== 'undefined') {
                if (params.request.url.includes('epica')) {
                    createdelement = "";
                    var requestDiv = document.createElement("div");
                    var requesthistoricDiv = document.createElement("div");
                    requestDiv.className = "request other";
                    requesthistoricDiv.className = "request other";
                    var urlLine = document.createElement("div");
                    var urlhistoricLine = document.createElement("div");
                    urlLine.className = "request-url";
                    urlhistoricLine.className = "request-url";
                    var str = params.request.url;
                    var titlestr = "";
                    if (str == "https://pixel.epica.ai/api/v1/js/p") {
                        titlestr = "Page";
                    }
                    else if (str == "https://pixel.epica.ai/api/v1/js/t") {
                        titlestr = "Track";
                    }
                    else if (str == "https://pixel.epica.ai/api/v1/js/i") {
                        titlestr = "Identify";
                    }
                    else if (str.includes("api/v1/recommender")) {
                        titlestr = "Recommender";
                    }

                    if (titlestr !== "") {

                        urlLine.innerHTML = titlestr;
                        urlhistoricLine.innerHTML = titlestr;

                        var headerContent = document.createElement("div");
                        headerContent.className = "header-content";
                        var responseTitle = document.createElement("div");
                        responseTitle.className = "response-title";

                        var headerhistoricContent = document.createElement("div");
                        headerhistoricContent.className = "header-content";
                        var responsehistoricTitle = document.createElement("div");
                        responsehistoricTitle.className = "response-title";

                        headerContent.appendChild(urlLine);
                        headerContent.appendChild(responseTitle);

                        headerhistoricContent.appendChild(urlhistoricLine);
                        headerhistoricContent.appendChild(responsehistoricTitle);

                        requestDiv.appendChild(headerContent);
                        requesthistoricDiv.appendChild(headerhistoricContent);

                        var requestTitle = document.createElement("div");
                        var requestContent = document.createElement("div");
                        var requestLine = document.createElement("div");
                        var requestmiddleLine = document.createElement("div");

                        var requesthistoricTitle = document.createElement("div");
                        var requesthistoricContent = document.createElement("div");
                        var requesthistoricLine = document.createElement("div");
                        var requesthistoricmiddleLine = document.createElement("div");

                        var warningtext = "";

                        requestLine.className = "request-line";
                        requestmiddleLine.className = "request-line";
                        requestTitle.className = "request-title";
                        requestContent.className = "request-content";

                        requesthistoricLine.className = "request-line";
                        requesthistoricmiddleLine.className = "request-line";
                        requesthistoricTitle.className = "request-title";
                        requesthistoricContent.className = "request-content";

                        if (typeof params.request.postData !== 'undefined') {
                            var jsondata;
                            jsondata = JSON.parse(params.request.postData);
                            var responsechar = "";
                            var responsetitlechar = "";
                            requestDiv.appendChild(requestLine);
                            requesthistoricDiv.appendChild(requesthistoricLine);
                            if (jsondata.type == "identify") {
                                requestDiv.className = "request identify";
                                requesthistoricDiv.className = "request identify";
                                reloadcount++;
                                historiccount++;
                                document.getElementById('count1').innerHTML = 'Count: ' + reloadcount;
                                document.getElementById('count2').innerHTML = 'Count: ' + historiccount;
                                if (typeof jsondata.traits !== 'undefined') {
                                    responsechar = 'Event Traits: <br />';
                                    Object.keys(jsondata.traits).forEach(function (key) {
                                        if (initialIsCapital(key)) {
                                            warningtext = warningtext + '<span>- Trait name is not standard.</span>';
                                        }
                                        responsechar = responsechar.concat(key + ': ' + jsondata.traits[key] + '<br />');
                                        if (jsondata.traits[key] == null) {
                                            warningtext = warningtext + '<span>- Value of ' + key + ' is null.</span>';
                                        }
                                        else if (jsondata.traits[key] == "") {
                                            warningtext = warningtext + '<span>- Value of ' + key + ' is empty.</span>';
                                        }
                                    })
                                }
                                else {
                                    warningtext = warningtext + '<span>- Traits not found.</span>';
                                }
                                createdelement = "identify";
                            }
                            else if (jsondata.type == "track") {
                                requestDiv.className = "request track";
                                requesthistoricDiv.className = "request track";
                                reloadcount++;
                                historiccount++;
                                document.getElementById('count1').innerHTML = 'Event Count: ' + reloadcount;
                                document.getElementById('count2').innerHTML = 'Event Count: ' + historiccount;
                                responsetitlechar = 'Event Name: ' + jsondata.event + '\n';
                                if (eventStandard(JSON.stringify(jsondata.event))) {
                                    warningtext = warningtext + '<span>- Event name is not standard. Did you mean ' + compareEvents(jsondata.event) + '?.</span>';
                                }
                                else if (!everyInitialIsCapital(JSON.stringify(jsondata.event))) {
                                    warningtext = warningtext + '<span>- Event name is not well formatted.</span>';
                                }
                                if (typeof jsondata.properties !== 'undefined') {
                                    responsechar = 'Event Properties: <br />';
                                    Object.keys(jsondata.properties).forEach(function (key) {
                                        if (initialIsCapital(key)) {
                                            warningtext = warningtext + '<span>- Property name is not standard.</span>';
                                        }
                                        if (key == "clusters") {
                                            var j = 0;
                                            var clusterarray = [];
                                            for (j = 0; j < jsondata.properties[key].length; j++) {
                                                clusterarray.push('<a href="' + clusterlink + jsondata.properties[key][j] + '" target="_blank">' + jsondata.properties[key][j] + '</a>');
                                            }
                                            responsechar = responsechar.concat(key + ': ' + clusterarray + '<br />');
                                        }
                                        else if (key == "products") {
                                            responsechar = responsechar.concat(key + ': ' + JSON.stringify(jsondata.properties[key]) + '<br />');
                                        }
                                        else {
                                            responsechar = responsechar.concat(key + ': ' + jsondata.properties[key] + '<br />');
                                        }
                                        if (jsondata.properties[key] == null) {
                                            warningtext = warningtext + '<span>- Value of ' + key + ' is null.</span>';
                                        }
                                        else if (jsondata.properties[key] == "") {
                                            if (key !== "clusters") {
                                                warningtext = warningtext + '<span>- Value of ' + key + ' is empty.</span>';
                                            }
                                        }
                                        createdelement = "track";
                                    })
                                }
                                requestTitle.innerHTML = responsetitlechar;
                                requestDiv.appendChild(requestTitle);
                                requestDiv.appendChild(requestmiddleLine);

                                requesthistoricTitle.innerHTML = responsetitlechar;
                                requesthistoricDiv.appendChild(requesthistoricTitle);
                                requesthistoricDiv.appendChild(requesthistoricmiddleLine);
                            }
                            else if (jsondata.type == "page") {
                                requestDiv.className = "request page";
                                requesthistoricDiv.className = "request page";
                                if (readytoberestored) {
                                    reloadcount = 0;
                                    readytoberestored = false;
                                }
                                reloadcount++;
                                historiccount++;
                                document.getElementById('count1').innerHTML = 'Count: ' + reloadcount;
                                document.getElementById('count2').innerHTML = 'Count: ' + historiccount;
                                instantiateContent(jsondata);
                                document.getElementById('absolute-layout').style.display = "none";
                                if (reloaded) {
                                    var resetcontainer = document.getElementById("container-left");
                                    resetcontainer.innerHTML = "";
                                    reloaded = false;
                                    countpage = 0;
                                }
                                if (countpage == 1) {
                                    warningtext = warningtext + '<span>- Epica Pixel is loaded more than one time.</span>';
                                }
                                countpage++;
                                Object.keys(jsondata.context.page).forEach(function (key) {
                                    responsechar = responsechar.concat(key + ': ' + jsondata.context.page[key] + '<br />');
                                })
                                createdelement = "page";
                                hasevents = true;
                            }
                            else {
                                createdelement = "other";
                            }
                            requestContent.innerHTML = '<p>' + responsechar + '</p>';
                            requestDiv.appendChild(requestContent);
                            requesthistoricContent.innerHTML = '<p>' + responsechar + '</p>';
                            requesthistoricDiv.appendChild(requesthistoricContent);
                            if (warningtext !== "") {
                                var warningElem = document.createElement("div");
                                var warningtextElem = document.createElement("div");

                                var warninghistoricElem = document.createElement("div");
                                var warninghistorictextElem = document.createElement("div");

                                var img = document.createElement("img");
                                img.setAttribute("src", "assets/warning-sign.png");

                                var historicimg = document.createElement("img");
                                historicimg.setAttribute("src", "assets/warning-sign.png");

                                warningElem.className = "tooltip";
                                warningtextElem.className = "tooltiptext";

                                warninghistoricElem.className = "tooltip";
                                warninghistorictextElem.className = "tooltiptext";

                                warningtextElem.innerHTML = warningtext;
                                warningElem.appendChild(img);
                                warningElem.appendChild(warningtextElem);
                                requestDiv.appendChild(warningElem);

                                warninghistorictextElem.innerHTML = warningtext;
                                warninghistoricElem.appendChild(historicimg);
                                warninghistoricElem.appendChild(warninghistorictextElem);
                                requesthistoricDiv.appendChild(warninghistoricElem);
                            }
                        }

                        var uniquerequestId = params.requestId.split('.').join("");

                        var list = document.getElementById("container-left");
                        requestDiv.setAttribute("id", "left" + uniquerequestId);
                        list.insertBefore(requestDiv, list.childNodes[0])

                        var listcopy = document.getElementById("container-right");
                        requesthistoricDiv.setAttribute("id", "right" + uniquerequestId);
                        listcopy.insertBefore(requesthistoricDiv, listcopy.childNodes[0]);

                        if (createdelement == "page") {
                            if (!document.getElementById("page-label").checked) {
                                var req = document.getElementById("left" + uniquerequestId);
                                req.style.display = 'none';

                                var req = document.getElementById("right" + uniquerequestId);
                                req.style.display = 'none';
                            }
                        }
                        else if (createdelement == "track") {
                            if (!document.getElementById("track-label").checked) {
                                var req = document.getElementById("left" + uniquerequestId);
                                req.style.display = 'none';

                                var req = document.getElementById("right" + uniquerequestId);
                                req.style.display = 'none';
                            }
                        }
                        else if (createdelement == "identify") {
                            if (!document.getElementById("identify-label").checked) {
                                var req = document.getElementById("left" + uniquerequestId);
                                req.style.display = 'none';

                                var req = document.getElementById("right" + uniquerequestId);
                                req.style.display = 'none';
                            }
                        }
                        else {
                            if (!document.getElementById("other-label").checked) {
                                var req = document.getElementById("left" + uniquerequestId);
                                req.style.display = 'none';

                                var req = document.getElementById("right" + uniquerequestId);
                                req.style.display = 'none';
                            }
                        }
                    }
                }
            }
        }
        else if (message == "Network.responseReceived") {
            var uniqueresponseId = params.requestId.split('.').join("");
            var leftelement = document.getElementById('left' + uniqueresponseId);
            var rightelement = document.getElementById('right' + uniqueresponseId);
            if (typeof leftelement !== 'undefined' && leftelement !== null) {
                var leftstatus = leftelement.childNodes[0].childNodes[1];
                leftstatus.innerHTML = params.response.status + ' ' + params.response.statusText;
                var rightstatus = rightelement.childNodes[0].childNodes[1];
                rightstatus.innerHTML = params.response.status + ' ' + params.response.statusText;
                if (params.response.status == 200) {
                    leftstatus.style.color = "green";
                    rightstatus.style.color = "green";
                }
                else if (params.response.status > 200 && params.response.status < 399) {
                    leftstatus.style.color = "orange";
                    rightstatus.style.color = "orange";
                }
                else if (params.response.status >= 300 && params.response.status < 399) {
                    leftstatus.style.color = "red";
                    rightstatus.style.color = "red";
                }
                if (typeof params.response.url !== 'undefined') {
                    if (params.response.url.includes('api/v1/recommender')) {
                        chrome.debugger.sendCommand({ tabId: debuggeeId.tabId }, "Network.getResponseBody", {
                            "requestId": params.requestId
                        }, function (response) {
                            var jsonViewer1 = new JSONViewer();
                            var jsonViewer2 = new JSONViewer();
                            if (typeof leftelement.childNodes[2] !== "undefined") {
                                leftelement.childNodes[2].appendChild(jsonViewer1.getContainer());
                                rightelement.childNodes[2].appendChild(jsonViewer2.getContainer());
                                if (typeof response.body !== 'undefined') {
                                    var responsebody = JSON.parse(response.body);
                                    jsonViewer1.showJSON(responsebody.buckets[0].items, null, 0);
                                    jsonViewer2.showJSON(responsebody.buckets[0].items, null, 0);
                                }
                                else {
                                    var responsebody = JSON.parse(response);
                                    jsonViewer1.showJSON(responsebody.buckets[0].items, null, 0);
                                    jsonViewer2.showJSON(responsebody.buckets[0].items, null, 0);
                                }
                            }

                        });
                    }
                }
            }
        }
    }
}

var checkboxI = document.querySelector("input[name=identifycheck]");

checkboxI.addEventListener('change', function () {
    if (this.checked) {
        var divsToHide = document.getElementsByClassName("identify");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "block";
        }
    } else {
        var divsToHide = document.getElementsByClassName("identify");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "none";
        }
    }
});

var checkboxT = document.querySelector("input[name=trackcheck]");

checkboxT.addEventListener('change', function () {
    if (this.checked) {
        var divsToHide = document.getElementsByClassName("track");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "block";
        }
    } else {
        var divsToHide = document.getElementsByClassName("track");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "none";
        }
    }
});

var checkboxP = document.querySelector("input[name=pagecheck]");

checkboxP.addEventListener('change', function () {
    if (this.checked) {
        var divsToHide = document.getElementsByClassName("page");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "block";
        }
    } else {
        var divsToHide = document.getElementsByClassName("page");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "none";
        }
    }
});

var checkboxO = document.querySelector("input[name=othercheck]");

checkboxO.addEventListener('change', function () {
    if (this.checked) {
        var divsToHide = document.getElementsByClassName("other");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "block";
        }
    } else {
        var divsToHide = document.getElementsByClassName("other");
        for (var i = 0; i < divsToHide.length; i++) {
            divsToHide[i].style.display = "none";
        }
    }
});

function everyInitialIsCapital(str) {
    str = str.substring(1, str.length - 1);
    var strarray = str.split(' ');
    var count = 0;
    for (var i = 0; i < strarray.length; i++) {
        if (strarray[i] !== strarray[i].toLowerCase()) {
            count++;
        }
    }
    return count == strarray.length;
}

function initialIsCapital(str) {
    if (str[0] === str[0].toUpperCase()) {
        return true;
    }
    else return false;
}

function eventStandard(eventname) {
    eventname = eventname.substring(1, eventname.length - 1);
    if (eventlist.indexOf(eventname) >= 0) {
        return false;
    }
    else return true;
}

function compareEvents(str) {
    var minimumvalue = 100;
    var minimumstring = "";
    var oneword = true;
    var value = 0;
    str = everywordtoUpper(str);
    var sArray = str.split(' ');
    var n = sArray.length;
    var wordArray = [];
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            if (i !== j) {
                wordArray.push(sArray[i] + " " + sArray[j]);
                oneword = false;
            }
        }
    }
    if (oneword) {
        wordArray.push(str);
    }
    for (var y = 0; y < wordArray.length; y++) {
        for (var z = 0; z < eventlist.length; z++) {
            value = stringDistance(wordArray[y], eventlist[z]);
            console.log("Compare: " + wordArray[y] + " = " + eventlist[z])
            if (minimumvalue > value) {
                minimumvalue = value;
                minimumstring = eventlist[z];
            }
        }
    }
    return minimumstring;
}

function everywordtoUpper(str) {
    return str
        .toLowerCase()
        .split(' ')
        .map(function (word) {
            return word[0].toUpperCase() + word.substr(1);
        })
        .join(' ');
}

function stringDistance(a, b) {
    if (a.length == 0) return b.length;
    if (b.length == 0) return a.length;
    var matrix = [];
    var i;
    for (i = 0; i <= b.length; i++) {
        matrix[i] = [i];
    }
    var j;
    for (j = 0; j <= a.length; j++) {
        matrix[0][j] = j;
    }
    for (i = 1; i <= b.length; i++) {
        for (j = 1; j <= a.length; j++) {
            if (b.charAt(i - 1) == a.charAt(j - 1)) {
                matrix[i][j] = matrix[i - 1][j - 1];
            } else {
                matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1,
                    Math.min(matrix[i][j - 1] + 1,
                        matrix[i - 1][j] + 1));
            }
        }
    }

    return matrix[b.length][a.length];
}

function instantiateContent(data) {
    write_key = data.writeKey;
    anonymous_id = data.anonymousId;
    if(key!=write_key || id!=anonymous_id){
        key=write_key;
        id=anonymous_id;        
    }    
    platformurl = platformurl + 'anonymous/' + anonymous_id + '/activity';

    var write_key_div;
    write_key_div = document.getElementById("key");
    write_key_div.innerHTML = 'Write Key: <span id="unique-warning">' + write_key + '</span>';
    console.log(write_key.length);
    if (write_key.length !== 32) {
        var warningwritetextElem = document.createElement("div");

        write_key_div.className = "tooltip-write";
        var key_color = document.getElementById("unique-warning");
        key_color.style.color = "orange";
        warningwritetextElem.className = "tooltiptext-write";

        var warningwritekey = "Not a valid write key";
        warningwritetextElem.innerHTML = warningwritekey;
        write_key_div.appendChild(warningwritetextElem);
    }

    document.getElementById("anonymous").innerHTML = 'Anonymous Id: ' + anonymous_id;

    if (typeof data.epicaSession !== 'undefined') {
        epica_session = data.epicaSession.sessionId;
        document.getElementById("status").innerHTML = 'Status: New Pixel';
    }
    else {
        document.getElementById("status").innerHTML = 'Status: Old Pixel';
    }
}

document.getElementById('button1').onclick = function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        var tab = tabs[0];
        chrome.tabs.create({ url: 'https://poderio.atlassian.net/servicedesk/customer/portal/1/topic/67253098-6acc-4759-b16e-bc7c2c0c7a87' });
    });
};

document.getElementById('button2').onclick = function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        var tab = tabs[0];
        chrome.tabs.create({ url: platformurl });
    });
};

document.getElementById('button3').onclick = function () {
    reloadMainTab();
};

document.getElementById('button4').onclick = function () {
    checkClusters(anonymous_id, write_key);
};

document.getElementById('close-cluster-modal').onclick = function () {
    document.getElementById('cluster-modal').style.display="none";
};

function reloadMainTab() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.reload(tabId);
    });
    initial_reload = true;
    findingEvents();
    setTimeout(eventsFound, 20000);
}

document.getElementById('reloadBtn').addEventListener('click', reloadMainTab);

function my_onkeydown_handler(event) {
    switch (event.keyCode) {
        case 116: // 'F5'
            event.preventDefault();
            event.keyCode = 0;
            window.status = "F5 disabled";
            break;
    }
}
document.addEventListener("keydown", my_onkeydown_handler);

function checkClusters(id, key){
    
    var xhr = new XMLHttpRequest();
    
    xhr.addEventListener("readystatechange", function() {
        if(this.readyState === 4) {
            var clusters=JSON.parse(this.responseText);

            if(typeof clusters !== 'undefined'){
                if(clusters.data !== ""){
                    var container=document.getElementById("cluster-container");
                    container.innerHTML="";
                    for (var i=0;i<clusters.data.length;i++){
                        container.innerHTML=container.innerHTML+'<p><a href="https://platform.epica.ai/cluster-builder/'+clusters.data[i]+'" target="_blank">'+clusters.data[i]+'</p>';
                    }
                }
            }
        }
    });

    xhr.open("GET", "https://api.epica.ai/api/v2/personas/"+id+"/cluster_ids?write_key="+key+"&identifier_name=anonymous_id");
    xhr.send();

    document.getElementById("cluster-modal").style.display="block";
}